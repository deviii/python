#!/bin/python3
import psutil
import time
import pandas as pd
import os

update_delay = 1

def get_size(bytes):
    if bytes == 0:
        return "0.00 B"
    for unit in [' ', 'K','M','G','T','P']:
        if bytes < 1024:
            return f"{bytes:.2f}{unit}B"
        bytes /= 1024
# Returns size of bytes in a nice format

io = psutil.net_io_counters(pernic=True)
#get the  network I/O stat from psutil

#extract the total bytes sent and received
def display_network_io():
        io = psutil.net_io_counters(pernic=True)
        time.sleep(update_delay)
        io_2 = psutil.net_io_counters(pernic=True)
        data = []
        for iface, iface_io in io.items():
            upload_speed = io_2[iface].bytes_sent - iface_io.bytes_sent
            download_speed =  io_2[iface].bytes_recv - iface_io.bytes_recv
            data.append({
                "iface" : iface, "Download" : get_size(io_2[iface].bytes_recv),
                "Upload" : get_size(io_2[iface].bytes_sent),
                "Upload Speed" : f"{get_size(upload_speed / update_delay)}/s",
                "Download Speed" : f"{get_size(download_speed / update_delay)}/s",
           })
        io = io_2
        df = pd.DataFrame(data)
        df.sort_values("Download", inplace = True, ascending = False)
        print(df.to_string())
        