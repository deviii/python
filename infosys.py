from abc import ABC, abstractmethod
from csv import writer
import csv
import os

classroom = []
lectors = []
courses = []
missing = []

class Person (ABC):
    def __init__(self, name, surname):
        self.name=name
        self.surname=surname

    @abstractmethod
    def introduce_yourself(self):
        pass

class Student(Person):
    def __init__(self, name, surname, grade):
        super().__init__(name, surname)
        self.grade = grade

    def introduce_yourself(self):
        return f"Hello, my name is {self.name} {self.surname}. I am student. My grade is {self.grade}."

class Teacher(Person):
    def __init__(self, name, surname, degree):
        super().__init__(name, surname)
        self.degree = degree

    def introduce_yourself(self):
        return f"Hello, I'm teacher {self.name} {self.surname}. I have {self.degree} degree."

class Course:
    def __init__(self, name_c, Student, Teacher):
        self.name_c = name_c
        self.Student =Student
        self.Teacher = Teacher

def make_student():
    n = input('Add name: ')
    s = input('Add surname: ')
    g = input('Add grade: ')
    student = Student(n, s, g)  # Corrected class name
    classroom.append((student.name, student.surname, student.grade))  # Appending name, surname, and grade

def make_teacher():
    n = input('Add name: ')
    s = input('Add surname: ')
    d = input('Add degree: ')
    teacher = Teacher(n, s, d)
    lectors.append((teacher.name, teacher.surname, teacher.degree))

def make_course():
    n =input('Add name: ')
    t = input('Add lector: ')
    s = input('Add student: ')
    course = Course(n,s,t)
    courses.append((course.name_c, course.Teacher, course.Student))

question = "0"

def add_f():
    question = input('Do you want to add student, course or teacher? For student enter "s", for course enter "c" and for teacher enter "t". If you want quit enter "q". \n your chose: ')

    match question:
        case "s":
            make_student()
        case "t":
            make_teacher()
        case "c":
            make_course()
        case "q":
            return 1
        case _:
            print ('For student enter "s", for course enter "c" and for teacher enter "t".\n your chose: ')
    if question != 1:
            add_f()
            

def recorder():
    
    f = os.path.exists('classroom.csv')
    
    match f:
        case False:
              with open ('classroom.csv','w',  newline = '') as file:
                  object = writer(file, delimiter=';')
                  object.writerows(classroom)
                  
        case True:
            with open('classroom.csv', 'r',newline='' ) as file:
                reader = csv.reader(file)
                existing = set()
                for row in reader:
                    existing.update(row)
                for value in classroom:
                    if value not in existing:
                        missing.append(value)
           
            with open ('classroom.csv', 'a', newline='') as file:
                object = writer(file, delimiter=';')
                object.writerows(missing)
add_f()
recorder()
print(missing)