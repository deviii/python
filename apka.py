import tkinter as tk
from tkinter import ttk

win = tk.Tk()
win.title('fahrenheit convert')
win.geometry('300x200')

def on_submit():
   try:
    f = ent.get()
    F = int(F)
    c = (F-32 )*5/9
    lbl_result['text'] = round(c)
   except:
    lbl_result['text']= 'Check input'

lbl_far =tk.Label(win, text = '\N{degree fahrenheit}')
lbl_cels=tk.Label(win, text = '\N{degree celsius}')
lbl_result = tk.Label(win,text='-')
ent = ttk.Entry(win, width=6)
btn = ttk.Button(win,text = 'convert',command=on_submit)

ent.grid(row=0,column=0, padx=10, pady=10)
lbl_far.grid(row=0,column=1)
btn.grid(row=0,column=2)
lbl_result.grid(row=1,column=0,padx = 10, pady=10)
lbl_cels.grid(row=1,column=1)

win.mainloop()